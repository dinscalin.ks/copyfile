package copyfile.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class CopyFile {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);  // решил сканер ибо  проще, так как перая 4х часовая практика не сохранилась...
        String pathFrom = scan.nextLine();          //TODO надо приучить себя сейвится в гите
        String fileName = String.valueOf(Path.of(pathFrom).getFileName());
        String buffer = fileName.substring(0, fileName.length()-4);
        String copyFileName = buffer + "_copy.txt";       // залипуха пилим название фала
        Path dir = Path.of(pathFrom).getParent();
        String pathTo = String.valueOf(dir) + copyFileName;
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy в HH:mm:ss"); // красивый формат
        String logTime = "" + formatter.format(now) + " был скопирован " + fileName + " в " + copyFileName + "\n";

        try {
            Path bytes = Files.copy(Path.of(pathFrom), Path.of(pathTo), REPLACE_EXISTING );
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (FileWriter writer = new FileWriter("log.txt", true)){    // 2 блока try по хорошему надо бы в один но происходят не понятк с
            writer.write(logTime);                                                          // закрытием writer
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





